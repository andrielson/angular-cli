# syntax=docker/dockerfile:experimental
FROM node:15.13.0-alpine3.13

COPY docker-entrypoint.sh /usr/local/bin

RUN chmod +x /usr/local/bin/docker-entrypoint.sh && \
    apk add --no-cache git tzdata && \
    cp --verbose /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime && \
    echo 'America/Sao_Paulo' | tee /etc/timezone && \
    apk del tzdata && \
    yarn --no-progress --silent global add @angular/cli
